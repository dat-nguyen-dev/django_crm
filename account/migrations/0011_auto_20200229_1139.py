# Generated by Django 3.0.3 on 2020-02-29 04:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0010_auto_20200229_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='account.Company'),
        ),
    ]
